# IMFit UI Tests

This project contains all the test scenarios intended to exercise the interactions with the IMFit WebApp.


### Prerequisites

[Java8](https://www.java.com/en/download/)
If you want to run the scenarios from command line, [Apache Maven](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html) is required. If not, you can use the embedded Maven plugin of an IDE such as Eclipse of IntelliJ IDEA. For more details, refer to Running the tests section.

If you want to run the scenarios using a local browser, below is the list of the currently supported ones:
- Chrome, using [ChromeDriver](https://chromedriver.chromium.org/downloads).
- Firefox, using [GeckoDriver](https://github.com/mozilla/geckodriver/releases).
- Safari, following the [official instructions](https://developer.apple.com/documentation/webkit/testing_with_webdriver_in_safari).
- Edge, using [Edge implementation for WebDriver](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/).
- Internet Explorer, using [IE WebDriver client](https://selenium-release.storage.googleapis.com/index.html).

Also, [Saucelabs](https://saucelabs.com/) is the cloud-hosted solution supported.

Important: Please refer to this [Confluence page](https://flexdigitalhealth.atlassian.net/wiki/spaces/AZORC/pages/1483375087/08+Environments) for information about Environment configs and user accounts.

Optional:
- An IDE of your choice. For this readme [Eclipse](https://www.eclipse.org/downloads/) was used. 


### Installing

Clone the repository
```
$ git clone git@bitbucket.org:flexdhdev/imfit-qa.git
```


### Running the tests
To run tests using a browser:
```
$ mvn clean test -DbrowserType=<Local or remote browser> -DresolutionHeight=<screen_height> -DresolutionWidth=<screen_width> -Dlang=default -D<browser_binary_path> -DsutUrl=<Environment> -Demail=<email_for_testing> -Dpassword=<password>
```

Where the variables are:

##### browserType
Use one of the following values:
- LOCAL_CHROME: This option employs the Chrome installed locally. It will require the variable webdriver.chrome.driver, also.
- REMOTE_CHROME: This option uses the dockerized Chrome, so before running the test with this option is required to run Docker Compose to load the container. The Dockerfile is not complete yet, due to the multiversion feature requested recently.
- SAUCELABS: This option runs the tests in the Saucelabs cloud. For more details refer to the following step.
- LOCAL_SAFARI: This option employs the Safari installed locally (Bear in mind this option requires MacOS). It will require the variable webdriver.chrome.driver, also.
- LOCAL_FIREFOX: This option employs the FF installed locally. It will require the variable webdriver.gecko.driver, also.
- LOCAL_EDGE: This option employs the MS Edge installed locally. It will require the variable webdriver.edge.driver, also.
- LOCAL_IE: This option employs the MS IE installed locally. It will require the variable webdriver.ie.driver, also.

##### resolutionHeight and resolutionWidth
Optional params. Define a set of values if the test cases are required to run with a screen size different than a maximized screen.
For instance: -DresolutionHeight=800 -DresolutionWidth=600.

##### browser_binary_path
According to the browserType selected, use one of the options below:
- webdriver.chrome.driver=/path/to/Chromedriver/chromedriver
- webdriver.gecko.driver=/path/to/Geckodriver/geckodriver (for Firefox)
- webdriver.edge.driver=/path/to/EdgeDriver.exe
- webdriver.ie.driver=/path/to/iexploredriver.exe

##### lang
The locators type and site language. This flag makes possible to inject different language related configs into the test cases at runtime.

##### sutUrl
The environment URL on which the test cases are going to run.

##### email
The email to log into the application

##### password
The password to log into the application


## Built With

* [WebDriver](https://www.selenium.dev/)
* [Hamcrest](http://hamcrest.org/JavaHamcrest/)
* [TestNG](https://testng.org/doc/)
