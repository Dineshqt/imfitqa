package com.imfit.tests;

import java.lang.reflect.Method;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.imfit.factory.BrowserFactory;
import com.imfit.global.GlobalData;
import com.imfit.helper.PathGenerator;
import com.imfit.helper.ResolutionPropertyParser;
import com.imfit.helper.ScreenshotGenerator;
import com.imfit.pageobject.CalculatorPageObject;
import com.imfit.pageobject.ConsentPageObject;
import com.imfit.pageobject.LoginPageObject;

/*
 * Test Case ID: # TBDs # 
 * Test Case Priority: # TBD # 
 * Tester Name: Sunil Kumar Shavanthgeri 
 * Test Creation Date : 09/23/2020 
 * Test Description - This class is to test the DOB Msg in Patient information page.
 */
public class TestDataValidationS2AgeLess18 {
	private static Logger logger = LoggerFactory.getLogger(TestGeneralConsent.class);
	private String testName;

	private BrowserFactory browserFactory;
	private ScreenshotGenerator screenshotGenerator;
	private PathGenerator pathGenerator;
	private ResolutionPropertyParser resolutionParser;

	private String browserType;
	private String sutUrl;
	private String lang;
	private String email;
	private String password;
	private String dob;
	private String patientId;
	private LoginPageObject loginPage;
	private ConsentPageObject consentPage;
	private CalculatorPageObject calculatorPage;

	/*
     * Test Method - setUp
     * Test Description - This method is used to initialize variables and browser settings.
     */
	@BeforeMethod
	public void setUp(Method method) {
		logger.info("============================================================");
		testName = method.getName();
		logger.info("Testcase name: " + testName);
		logger.info("============================================================");

		browserType = System.getProperty("browserType");
		logger.info("Test setUp() - Browser defined in env. variable: " + browserType);

		sutUrl = System.getProperty("sutUrl");
		logger.info("Test setUp() - SUT Url defined in env. variable: " + sutUrl);

		lang = System.getProperty("lang");
		logger.info("Language selected: " + lang);

		// dob = LocalDate.now().plusYears(-17).toString();
		dob = "10/25/2005";
		logger.info("Test setUp() - date of birth: " + dob);

		patientId = "1";
		logger.info("Test setUp() - patient Id: " + patientId);

		email = System.getProperty("email");
		logger.info("Test setUp() - email defined in env. variable: " + email);

		password = System.getProperty("password");
		logger.info("Test setUp() - password defined in env. variable: " + password);

		browserFactory = new BrowserFactory();

		resolutionParser = new ResolutionPropertyParser(System.getProperty("resolutionHeight"),
				System.getProperty("resolutionWidth"));

		int resolutionHeight = resolutionParser.toInteger().get(0);
		int resolutionWidth = resolutionParser.toInteger().get(1);

		if (resolutionHeight == 0 && resolutionWidth == 0) {
			loginPage = new LoginPageObject(browserFactory.create(browserType).getDefaultVersion(), lang);
		} else {
			loginPage = new LoginPageObject(browserFactory.create(browserType)
					.getDefaultVersionUsingResolution(resolutionHeight, resolutionWidth), lang);
		}

		pathGenerator = new PathGenerator();
		screenshotGenerator = new ScreenshotGenerator();
	}

	/*
     * Test Method - verifyDOBErrorMsg
     * Developed By - Qualitest
     * Test Description - This method is to test the DOB Msg after entering the invalid DOB.
     */
	@Test
	public void verifyDOBErrorMsg() {

		String screenshotPath = pathGenerator.setAbsolutePathFrom(GlobalData.SCREENSHOT_FOLDER_NAME)
				.createAddingDateTime(testName);

		
		consentPage = loginPage.open(sutUrl).signIn(email, password);
		consentPage.clickUserAccount().clickViewConsent().clickConsentAndContinueButton().clickUserAccount()
				.clickTermsAndConditions().clickAcceptAndContinue();
		calculatorPage = consentPage.clickGetStarted();
		String dobMsg = calculatorPage.selectTumorType().clickTumorTypeNextButton().setDateOfBirth(dob)
				.setPatientID(patientId).verifyDOBMsg();

		screenshotGenerator.setDriver(calculatorPage.getBrowserInstance());
		screenshotGenerator.saveIn(screenshotPath);

		Assert.assertEquals(dobMsg, "invalid-feedback");
	}

	/*
     * Test Method - tearDown
     * Test Description - This method is used to close the browser.
     */
	@AfterMethod
	public void tearDown() {
		loginPage.close();
	}
}
