package com.imfit.tests;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.imfit.factory.BrowserFactory;
import com.imfit.global.GlobalData;
import com.imfit.helper.PathGenerator;
import com.imfit.helper.ResolutionPropertyParser;
import com.imfit.helper.ScreenshotGenerator;
//import com.imfit.pageobject.ConsentPageObject;
import com.imfit.pageobject.LoginPageObject;

/*
 * Test Case ID: # TBDs # 
 * Test Case Priority: # TBD # 
 * Tester Name: Sunil Kumar Shavanthgeri 
 * Test Creation Date : 09/28/2020 
 * Description - This class is to validate the login.
 */
public class TestLoginPagePasswordRevealed {

	private static Logger logger = LoggerFactory.getLogger(TestLoginPagePasswordRevealed.class);
	private String testName;

	private BrowserFactory browserFactory;
	private ScreenshotGenerator screenshotGenerator;
	private PathGenerator pathGenerator;
	private ResolutionPropertyParser resolutionParser;

	private String browserType;
	private String sutUrl;
	private String lang;
	private String email;
	private String password;
	private LoginPageObject loginPage;
	// private ConsentPageObject consentPage;

	/*
     * Test Method - setUp
     * Test Description - This method is used to initialize variables and browser settings.
     */
	@BeforeMethod
	public void setUp(Method method) {
		logger.info("============================================================");
		testName = method.getName();
		logger.info("Testcase name: " + testName);
		logger.info("============================================================");

		browserType = System.getProperty("browserType");
		logger.info("Test setUp() - Browser defined in env. variable: " + browserType);

		sutUrl = System.getProperty("sutUrl");
		logger.info("Test setUp() - SUT Url defined in env. variable: " + sutUrl);

		lang = System.getProperty("lang");
		logger.info("Language selected: " + lang);

		email = System.getProperty("email");
		logger.info("Test setUp() - email defined in env. variable: " + email);
		password = System.getProperty("password");
		logger.info("Test setUp() - password defined in env. variable: " + password);

		browserFactory = new BrowserFactory();

		resolutionParser = new ResolutionPropertyParser(System.getProperty("resolutionHeight"),
				System.getProperty("resolutionWidth"));

		int resolutionHeight = resolutionParser.toInteger().get(0);
		int resolutionWidth = resolutionParser.toInteger().get(1);

		if (resolutionHeight == 0 && resolutionWidth == 0) {
			loginPage = new LoginPageObject(browserFactory.create(browserType).getDefaultVersion(), lang);
		} else {
			loginPage = new LoginPageObject(browserFactory.create(browserType)
					.getDefaultVersionUsingResolution(resolutionHeight, resolutionWidth), lang);
		}
		pathGenerator = new PathGenerator();
		screenshotGenerator = new ScreenshotGenerator();
	}

	/*
     * Test Method - verifyPasswordReveled
     * Developed By - Qualitest
     * Test Description - This method is to verify that the password will be seen after clicking the eye icon.
     */
	@Test
	public void verifyPasswordReveled() {
		String attributeValueExpected = "text";
		String screenshotPath = pathGenerator.setAbsolutePathFrom(GlobalData.SCREENSHOT_FOLDER_NAME)
				.createAddingDateTime(testName);

		loginPage.open(sutUrl).setEmail(email).setPassword(password).clickEyeIcon();
		String attributeValue = loginPage.getPasswordAttribute("type");

		screenshotGenerator.setDriver(loginPage.getBrowserInstance());
		screenshotGenerator.saveIn(screenshotPath);

		Assert.assertEquals(attributeValue, attributeValueExpected, "Not able to see the password text");

	}

	/*
     * Test Method - tearDown
     * Test Description - This method is used to close the browser.
     */
	@AfterMethod
	public void tearDown() {
		loginPage.close();
	}

}
