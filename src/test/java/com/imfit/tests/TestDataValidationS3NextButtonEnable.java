package com.imfit.tests;

import static org.testng.Assert.assertEquals;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.imfit.factory.BrowserFactory;
import com.imfit.global.GlobalData;
import com.imfit.helper.PathGenerator;
import com.imfit.helper.ResolutionPropertyParser;
import com.imfit.helper.ScreenshotGenerator;
import com.imfit.pageobject.CalculatorPageObject;
import com.imfit.pageobject.ConsentPageObject;
import com.imfit.pageobject.LoginPageObject;

/*
 * Test Case ID: # TBDs # 
 * Test Case Priority: # TBD # 
 * Tester Name: Silpa Vuppala. 
 * Test Creation Date : 09/22/2020 
 * Test Description - This class is to validate the next button in  Date & Indication page.
 */
public class TestDataValidationS3NextButtonEnable {

	private static Logger logger = LoggerFactory.getLogger(TestDataValidationS3NextButtonEnable.class);
	private String testName;

	private BrowserFactory browserFactory;
	private ScreenshotGenerator screenshotGenerator;
	private PathGenerator pathGenerator;
	private ResolutionPropertyParser resolutionParser;

	private String browserType;
	private String sutUrl;
	private String lang;
	private String email;
	private String password;
	private LoginPageObject loginPage;
	private ConsentPageObject consentPage;
	private CalculatorPageObject calculatorPage;

	/*
     * Test Method - setUp
     * Test Description - This method is used to initialize variables and browser settings.
     */
	@BeforeMethod
	public void setUp(Method method) {
		logger.info("============================================================");
		testName = method.getName();
		logger.info("Testcase name: " + testName);
		logger.info("============================================================");

		browserType = System.getProperty("browserType");
		logger.info("Test setUp() - Browser defined in env. variable: " + browserType);

		sutUrl = System.getProperty("sutUrl");
		logger.info("Test setUp() - SUT Url defined in env. variable: " + sutUrl);

		lang = System.getProperty("lang");
		logger.info("Language selected: " + lang);

		email = System.getProperty("email");
		logger.info("Test setUp() - email defined in env. variable: " + email);

		password = System.getProperty("password");

		logger.info("Test setUp() - password defined in env. variable: " + password);

		browserFactory = new BrowserFactory();

		resolutionParser = new ResolutionPropertyParser(System.getProperty("resolutionHeight"),
				System.getProperty("resolutionWidth"));

		int resolutionHeight = resolutionParser.toInteger().get(0);
		int resolutionWidth = resolutionParser.toInteger().get(1);

		if (resolutionHeight == 0 && resolutionWidth == 0) {
			loginPage = new LoginPageObject(browserFactory.create(browserType).getDefaultVersion(), lang);
		} else {
			loginPage = new LoginPageObject(browserFactory.create(browserType)
					.getDefaultVersionUsingResolution(resolutionHeight, resolutionWidth), lang);
		}

		pathGenerator = new PathGenerator();
		screenshotGenerator = new ScreenshotGenerator();
	}

	/*
     * Test Method - verifyNextButtonIsEnable
     * Developed By - Qualitest
     * Test Description - This method is to test the next button is enable after entering all valid information in Date & Indication page.
     */
	@Test
	public void verifyNextButtonIsEnable() {
		String screenshotPath = pathGenerator.setAbsolutePathFrom(GlobalData.SCREENSHOT_FOLDER_NAME)
				.createAddingDateTime(testName);

		consentPage = loginPage.open(sutUrl).signIn(email, password);
		consentPage.clickUserAccount();
		consentPage.clickViewConsent();
		consentPage.clickConsentAndContinueButton();
		consentPage.clickUserAccount();
		consentPage.clickTermsAndConditions();
		consentPage.clickAcceptAndContinue();
		calculatorPage = consentPage.clickGetStarted();
		calculatorPage.selectTumorType();
		calculatorPage.clickTumorTypeNextButton();
		calculatorPage.setFirstNameTextBox("John");
		calculatorPage.setLastNameTextBox("Doe");
		calculatorPage.setDateOfBirth("08/16/1983");
		calculatorPage.setPatientID("1");
		calculatorPage.clickNextButton();
		calculatorPage.clickBloodTest();
		calculatorPage.clickIndicationStatement();
		screenshotGenerator.setDriver(calculatorPage.getBrowserInstance());
		screenshotGenerator.saveIn(screenshotPath);
		assertEquals(calculatorPage.verifyNextButton(), true);

	}

	/*
     * Test Method - tearDown
     * Test Description - This method is used to close the browser.
     */
	@AfterClass
	public void tearDown() {
		loginPage.close();
	}
}
