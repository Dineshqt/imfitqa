package com.imfit.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imfit.pageobject.locator.CaluclatorLocator;
import com.imfit.pageobject.locator.Locators;

public class CalculatorPageObject {
	private static Logger logger = LoggerFactory.getLogger(ConsentPageObject.class);

	private WebDriver driver;
	private String lang;
	private CaluclatorLocator locators;

	public CalculatorPageObject(WebDriver driver, String lang) {
		this.driver = driver;
		this.lang = lang;
		this.locators = Locators.selectCaluclator(lang);
		this.locators.setDriver(driver);
	}

	public String getCurrentLang() {
		logger.info("Current lang: " + lang);

		return lang;
	}

	public WebDriver getBrowserInstance() {
		return this.driver;
	}

	public String getCurrentUrlContaining(String key) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.urlContains(key));

		String url = driver.getCurrentUrl();
		logger.info("Current URL: " + url);

		return url;
	}

	public CalculatorPageObject selectTumorType() {
		locators.getTumorType().click();
		return this;

	}

	public String verifyTumorTypeNextButton() {
		String verify = locators.getTumorTypeNext().getAttribute("innerHTML");
		return verify;
	}

	public CalculatorPageObject clickTumorTypeNextButton() {
		locators.getTumorTypeNext().click();
		return this;
	}

	public CalculatorPageObject setFirstNameTextBox(String firstname) {
		locators.getFirstName().sendKeys(firstname);
		return this;
	}

	public CalculatorPageObject setLastNameTextBox(String lastname) {
		locators.getLastName().sendKeys(lastname);
		return this;
	}

	public CalculatorPageObject setDateOfBirth(String dateofbirth) {
		locators.getDateBirth().sendKeys(dateofbirth);
		return this;
	}

	public CalculatorPageObject setPatientID(String patientid) {
		locators.getPatientId().sendKeys(patientid);
		return this;
	}

	public void clickNextButton() {
		locators.getNextButton().click();
	}

	public void clickBloodTest() {
		locators.getBloodTestConfirmation().click();
	}

	public void clickIndicationStatement() {
		locators.getIndicationStatement().click();
	}

	public boolean verifyNextButton() {

		return locators.getNextButton().isEnabled();

	}

	public void setLYM(String lym) {
		locators.getLYM().sendKeys(lym);
	}

	public void setNEUT(String neut) {
		locators.getNEUT().sendKeys(neut);
	}

	public void setALB(String alb) {
		locators.getALB().sendKeys(alb);
	}

	public void setAST(String ast) {
		locators.getAST().sendKeys(ast);
	}

	public void setGGT(String ggt) {
		locators.getGGT().sendKeys(ggt);
	}

	public void setLDH(String ldh) {
		locators.getLDH().sendKeys(ldh);
	}

	public boolean verifyViewResult() {
		return locators.getViewResult().isEnabled();
	}

	public String verifyDOBMsg() {
		String name = locators.getDateofBirthMsg().getAttribute("class");
		return name;

	}
}
