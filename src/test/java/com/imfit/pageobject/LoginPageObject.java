package com.imfit.pageobject;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imfit.pageobject.locator.Locators;
import com.imfit.pageobject.locator.LoginLocators;

public class LoginPageObject {
	private static Logger logger = LoggerFactory.getLogger(LoginPageObject.class);

	private WebDriver driver;
	private String lang;
	private LoginLocators locators;

	public LoginPageObject(WebDriver driver, String lang) {
		this.driver = driver;
		this.lang = lang;
		this.locators = Locators.selectLogin(lang);
		this.locators.setDriver(driver);
	}

	public String getCurrentLang() {
		logger.info("Current lang: " + lang);
		return lang;
	}

	public LoginPageObject open(String url) {
		logger.info("Opening URL: " + url);

		driver.get(url);
		return this;
	}

	public String getCurrentUrl() {
		String url = driver.getCurrentUrl();
		logger.info("Current URL: " + url);
		return url;
	}

	public ConsentPageObject signIn(String email, String password) {
		locators.getEmailField().sendKeys(email);
		locators.getPasswordField().sendKeys(password);
		locators.getSignInButton().click();
		return new ConsentPageObject(driver, lang);
	}

	public LoginPageObject wrongSignIn(String email, String password) {
		locators.getEmailField().sendKeys(email);
		locators.getPasswordField().sendKeys(password);
		locators.getSignInButton().click();
		return this;
	}

	public String getSignInLabelText() {
		String text = locators.getSignInLabel().getText();
		logger.info("Current Text: " + text);
		return text;
	}

	public String getIncorrectPassword() {
		String text = locators.getIncorrectPassword().getText();
		logger.info("Current Text: " + text);
		return text;
	}

	
	public LoginPageObject setEmail(String email) {
		locators.getEmailField().sendKeys(email);
		return this;
	}

	public LoginPageObject setPassword(String password) {
		locators.getPasswordField().sendKeys(password);
		return this;
	}

	public LoginPageObject clickEyeIcon() {
		locators.getShowPassword().click();
		return this;
	}

	public String getPasswordAttribute(String attributeName) {
		String attributeValue = locators.getPasswordField().getAttribute(attributeName);
		return attributeValue;
	}

	public String getErrorMsg() {
		String errorMsg = locators.getErrorMessage().getText();
		return errorMsg;
	}

	
	public WebDriver getBrowserInstance() {
		return this.driver;
	}

	public void close() {
		logger.info("Closing browser");
		driver.close();
	}


	public String getDescImFitText() {
		String text = locators.getDescImFit().getText();
		logger.info("Current Text: " + text);
		return text;
	}

	public String getDescSigninAccountText() {
		String text = locators.getDescSigninAccount().getText();
		logger.info("Current Text: " + text);
		return text;

	}

	public String getLabelEnterCredentialsToProceedText() {
		String text = locators.getLabelEnterCredentialsToProceed().getText();
		logger.info("Current Text: " + text);
		return text;

	}

	public String getLabelEmailAddressText() {
		String text = locators.getLabelEmailAddress().getText();
		logger.info("Current Text: " + text);
		return text;

	}

	public String getLabelPasswordText() {
		String text = locators.getLabelPassword().getText();
		logger.info("Current Text: " + text);
		return text;

	}

}
