package com.imfit.pageobject;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.imfit.pageobject.locator.ConsentLocators;
import com.imfit.pageobject.locator.Locators;

public class ConsentPageObject {
	private static Logger logger = LoggerFactory.getLogger(ConsentPageObject.class);
	
	private WebDriver driver;
	private String lang;
	private ConsentLocators locators;
	
	public ConsentPageObject(WebDriver driver, String lang) {
		this.driver = driver;
		this.lang = lang;
		this.locators = Locators.selectConsent(lang);
		this.locators.setDriver(driver);
	}
	
	public String getCurrentLang() {
		logger.info("Current lang: " + lang);
		
		return lang;
	}
	
	public String getCurrentUrlContaining(String key) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.urlContains(key));
		
		String url = driver.getCurrentUrl();
		logger.info("Current URL: " + url);
		
		return url;
	}
	
	public WebDriver getBrowserInstance() {
		return this.driver;
	}
	
	//qt code
	public ConsentPageObject clickUserAccount()
	{
		Actions action = new Actions(driver);
		action.moveToElement(locators.getUserAccount()).build().perform();
		return this;
	}
	
	public ConsentPageObject clickLanguages()
	{
		Actions action = new Actions(driver);
		action.moveToElement(locators.getLanguage()).build().perform();
		return this;
	}
	
	public List<String> getLanguages() {
		List<String> elementArray=new ArrayList<String>();
		for (WebElement element : locators.getLanguages()) {
			elementArray.add(element.getAttribute("innerHTML"));
		}
		return elementArray;
	}
	
	public ConsentPageObject clickViewConsent() {
		locators.getViewConsent().click();
		return this;
	}
	
	public ConsentPageObject clickTermsAndConditions() {
		locators.getTermsConditions().click();
		return this;
	}
	
	public ConsentPageObject clickConsentAndContinueButton() {
		
		locators.getConsentandContinue().click();
		return this;
		
	}
	
	public ConsentPageObject clickInstructionForUse() {
		locators.getInstructions().click();
		return this;
	}
	
	public String verifyInstructionForUse() {
		
		return locators.getInstructions().getAttribute("class");
	}
	
	public void clickAcceptAndContinue() {
		locators.getAcceptAndContinue().click();
	}
	public boolean verifyLogoutButton() {
		
		
		boolean VerifyLogout=locators.getLogoutButton().isDisplayed();
		return VerifyLogout;
	}
	
	public String getParagraphOne() {
		return locators.getParagraphOne().getText();
	}
	
	public String getWelcomeMsgText()
	{
		String Msg=locators.getWelcomeMsg().getText();
		return Msg;
	}
	
	public String getSelectedLanguage() {
		
		return locators.getEnglish().getText();
	}
		
	public CalculatorPageObject clickGetStarted() {
		locators.getGetStarted().click();
		return new CalculatorPageObject(driver,lang);
		
	}
	
	public String getConsentMsgText()
	{
		String Msg=locators.getConsentMsg().getText();
		return Msg;
	}
}
