package com.imfit.pageobject.locator;

import java.util.List;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsentDefaultLocators implements ConsentLocators {
	private static Logger logger = LoggerFactory.getLogger(ConsentDefaultLocators.class);
	private WebDriver driver;

	// qt code
	private By userAccount = By.xpath("(//i[@class='arrow down'])[2]");
	private By logoutButton = By.xpath("//a[text()='Log out']");
	private By welcomeMsg = By.xpath("//h1[text()='Welcome']");
	private By viewConsent = By.xpath("//a[text()='View Consent']");
	private By consentMsg = By.xpath("//h1[text()='Consent']");
	private By consentAndContinueButton = By.xpath("//span[text()='Consent and Continue ']");
	private By termsAndCondition = By.xpath("//li[3]/a[text()='Terms & Conditions']");
	private By acceptAndContinueButton = By.xpath("//span[text()='Accept and Continue ']");
	private By paragraphOne = By.xpath("//div[2]/div/p[1]");
	private By instructionForUse = By.xpath("//div[text()=' Instructions for Use ']");
	private By selectedLanguage = By.xpath("//div/div[2]/div/div");
	private By selectLanguagesDrpDwn = By.xpath("//span/i");
	private By selectableLanguages = By.xpath("//div/div/ul[@class='dropdown-menu']/li/a");
	private By getStarted=By.xpath("//span[text()='Get started ']");

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	@Override
	public WebElement getUserAccount() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(userAccount));

		logger.info("User Account: " + userAccount);

		return driver.findElement(userAccount);
	}

	@Override
	public WebElement getLogoutButton() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(logoutButton));

		logger.info("Logout Button: " + logoutButton);
		return driver.findElement(logoutButton);
	}

	@Override
	public WebElement getWelcomeMsg() {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(welcomeMsg));

		logger.info("Welcome Msg: " + welcomeMsg);
		return driver.findElement(welcomeMsg);
	}

	@Override
	public WebElement getViewConsent() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(viewConsent));
		logger.info("View Consent: " + viewConsent);
		return driver.findElement(viewConsent);
	}

	@Override
	public WebElement getConsentandContinue() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.elementToBeClickable(consentAndContinueButton));
		logger.info("Consent And Continue: " + consentAndContinueButton);
		return driver.findElement(consentAndContinueButton);
	}

	@Override
	public WebElement getTermsConditions() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(termsAndCondition));
		return driver.findElement(termsAndCondition);
	}

	@Override
	public WebElement getAcceptAndContinue() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(acceptAndContinueButton));
		return driver.findElement(acceptAndContinueButton);
	}

	@Override
	public WebElement getWelcomeMessege() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(welcomeMsg));

		return driver.findElement(welcomeMsg);
	}

	@Override
	public WebElement getParagraphOne() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(paragraphOne));
		return driver.findElement(paragraphOne);
	}

	@Override
	public WebElement getInstructions() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(instructionForUse));
		return driver.findElement(instructionForUse);
	}

	@Override
	public WebElement getEnglish() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(selectedLanguage));
		return driver.findElement(selectedLanguage);
		
	}

	@Override
	public WebElement getLanguage() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(selectLanguagesDrpDwn));
		return driver.findElement(selectLanguagesDrpDwn);
	}

	@Override
	public List<WebElement> getLanguages() {

		return driver.findElements(selectableLanguages);
	}

	@Override
	public WebElement getGetStarted() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(getStarted));
		return driver.findElement(getStarted);
	}

	@Override
	public WebElement getConsentMsg() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(consentMsg));
		
		logger.info("consentMsg locator: " + consentMsg);
		return driver.findElement(consentMsg);
	}

}
