package com.imfit.pageobject.locator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoginDefaultLocators implements LoginLocators {
	private static Logger logger = LoggerFactory.getLogger(LoginDefaultLocators.class);

	private WebDriver driver;

	private By emailField = By.id("email");
	private By passwordField = By.id("password");
	private By signInButton = By.className("sign-in");
	private By signInLabel = By.className("title");
	private By incorrectPassword = By.cssSelector(".validation-middle");
	private By showPassword = By.xpath("//a[@class='toggle-pwd']/span");
	private By errorMsg = By.xpath("//div[@class='validation-middle']");
	private By getImFitDesc = By.xpath("//div[contains(text(),'IMFit')]");
	private By signInToAccountDesc = By.xpath("//div[@class = 'title']");
	private By enterCredentialsDesc = By.xpath("//div[@class='description']");
	private By emailAddressLabel = By.xpath("//label[@for='email']");
	private By passwordLabel = By.xpath("//label[@for='password']");

	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}

	@Override
	public WebElement getEmailField() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(emailField));

		logger.info("Email field locator: " + emailField);

		return driver.findElement(emailField);
	}

	@Override
	public WebElement getPasswordField() {
		logger.info("Password field locator: " + passwordField);

		return driver.findElement(passwordField);
	}

	@Override
	public WebElement getSignInButton() {
		logger.info("SignIn button locator: " + signInButton);

		return driver.findElement(signInButton);
	}

	@Override
	public WebElement getSignInLabel() {
		logger.info("SignIn label locator: " + signInLabel);

		return driver.findElement(signInLabel);

	}

	@Override
	public WebElement getIncorrectPassword() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(incorrectPassword));

		logger.info("IncorrectPassword label locator: " + incorrectPassword);

		return driver.findElement(incorrectPassword);

	}

	@Override
	public WebElement getShowPassword() {

		logger.info("ShowPassword label locator: " + showPassword);

		return driver.findElement(showPassword);

	}

	@Override
	public WebElement getErrorMessage() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(errorMsg));

		logger.info("ErrorMsg label locator: " + errorMsg);

		return driver.findElement(errorMsg);
	}

	@Override
	public WebElement getDescImFit() {
		logger.info("ImFit Label Locator: " + getImFitDesc);

		return driver.findElement(getImFitDesc);

	}

	@Override
	public WebElement getDescSigninAccount() {
		logger.info("Sign in to Account Locator: " + signInToAccountDesc);

		return driver.findElement(signInToAccountDesc);

	}

	@Override
	public WebElement getLabelEnterCredentialsToProceed() {
		logger.info("Please enter your credentials to proceed Message Locator" + enterCredentialsDesc);

		return driver.findElement(enterCredentialsDesc);

	}

	@Override
	public WebElement getLabelEmailAddress() {
		logger.info("Email Address Label Locator" + emailAddressLabel);

		return driver.findElement(emailAddressLabel);

	}

	@Override
	public WebElement getLabelPassword() {
		logger.info("Password Label Locator" + passwordLabel);

		return driver.findElement(passwordLabel);

	}

}
