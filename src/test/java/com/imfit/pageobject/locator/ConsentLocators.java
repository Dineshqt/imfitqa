package com.imfit.pageobject.locator;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface ConsentLocators {
	public void setDriver(WebDriver driver);
	public WebElement getUserAccount();
	public WebElement getLogoutButton();
	public WebElement getWelcomeMsg();
	public WebElement getViewConsent();
	public WebElement getConsentandContinue();
	public WebElement getTermsConditions();
	public WebElement getAcceptAndContinue();
	public WebElement getWelcomeMessege();
	public WebElement getParagraphOne();
	public WebElement getInstructions();
	public WebElement getEnglish();
	public WebElement getLanguage();
	public List<WebElement> getLanguages();
	public WebElement getGetStarted();
	public WebElement getConsentMsg();
}
