package com.imfit.pageobject.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface CaluclatorLocator {
	public void setDriver(WebDriver driver);
	public WebElement getTumorTypeNext();
	public WebElement getTumorType();
	public WebElement getFirstName();
	public WebElement getLastName();
	public WebElement getDateBirth();
	public WebElement getPatientId();
	public WebElement getNextButton();
	public WebElement getBloodTestConfirmation();
	public WebElement getIndicationStatement();
	public WebElement getLYM();
	public WebElement getNEUT();
	public WebElement getALB();
	public WebElement getAST();
	public WebElement getGGT();
	public WebElement getLDH();
	public WebElement getViewResult();
	public WebElement getDateofBirthMsg();
	  

}
