package com.imfit.pageobject.locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public interface LoginLocators {
	public void setDriver(WebDriver driver);
	public WebElement getEmailField();
	public WebElement getPasswordField();
	public WebElement getSignInButton();
	public WebElement getSignInLabel();
	public WebElement getIncorrectPassword();
	public WebElement getShowPassword();
	public WebElement getErrorMessage();
	public WebElement getDescImFit();
	public WebElement getDescSigninAccount();
	public WebElement getLabelEnterCredentialsToProceed();
	public WebElement getLabelEmailAddress();
	public WebElement getLabelPassword();
	
	
}
