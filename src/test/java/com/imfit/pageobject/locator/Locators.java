package com.imfit.pageobject.locator;

import java.util.HashMap;
import java.util.Map;

public class Locators {
	public static LoginLocators selectLogin(String type) {
		Map<String, LoginLocators> locators = new HashMap<String, LoginLocators>();
		
		locators.put("default", new LoginDefaultLocators());
		
		return locators.get(type);
	}
	
	public static ConsentLocators selectConsent(String type) {
		Map<String, ConsentLocators> locators = new HashMap<String, ConsentLocators>();
		
		locators.put("default", new ConsentDefaultLocators());
		
		return locators.get(type);
	}
	public static CaluclatorLocator selectCaluclator(String type) {
		Map<String, CaluclatorLocator> locators = new HashMap<String, CaluclatorLocator>();
		
		locators.put("default", new CaluclatorDefaultLocators());
		
		return locators.get(type);
	}
}
