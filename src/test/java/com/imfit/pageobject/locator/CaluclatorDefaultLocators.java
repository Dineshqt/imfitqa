package com.imfit.pageobject.locator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CaluclatorDefaultLocators implements CaluclatorLocator {
	private static Logger logger = LoggerFactory.getLogger(ConsentDefaultLocators.class);
	private WebDriver driver;

	private By tumorType = By.xpath("//img[@class='float-left']");
	private By tumorTypeNext = By.xpath("//button[@class='btn btn-primary btn-icon-split Primary']");
	private By firstName=By.xpath("//input[@name='firstName']");
	private By lastName=By.xpath("//input[@name='lastName']");
	private By dateOfBirth=By.xpath("//input[@name='dateBirth']");
	private By dateofBirthMsg= By.xpath("//div[contains(@class,'feedback')]");
	private By patientId=By.xpath("//input[@name='patientId']");
	private By nextButton=By.xpath("//div/button[@tabindex='0']");
	private By viewResult=By.xpath("//div/button[@tabindex='0']");
	private By bloodTest=By.xpath("//div[2]/div[2]/label/img[@class='float-left']");
	private By indicationStatement=By.xpath("//div[7]/div[2]/label");
	private By valueLYM=By.xpath("//input[@placeholder='LYM 10⁹/L']");
	private By valueNEUT=By.xpath("//input[@placeholder='NEUT 10⁹/L']");
	private By valueALB=By.xpath("//input[@placeholder='ALB g/L']");
	private By valueAST=By.xpath("//input[@placeholder='AST IU/L']");
	private By valueGGT=By.xpath("//input[@placeholder='GGT IU/L']");
	private By valueLDH=By.xpath("//input[@placeholder='LDH IU/L']");
	
	@Override
	public void setDriver(WebDriver driver) {
		// TODO Auto-generated method stub
		this.driver = driver;
	}
	public WebElement getTumorType() {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(tumorType));
		
		logger.info("tumorType locator: " + tumorType);
		return driver.findElement(tumorType);
	}
	public WebElement getTumorTypeNext() {

		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(tumorTypeNext));
		
		logger.info("tumorTypeNext locator: " + tumorTypeNext);
		return driver.findElement(tumorTypeNext);
	}
	@Override
	public WebElement getFirstName() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(firstName));
		logger.info("First Name Box: " + firstName);
		return driver.findElement(firstName);
	}
	@Override
	public WebElement getLastName() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(firstName));
		logger.info("Last Name Box: " + lastName);
		return driver.findElement(lastName);
	}
	@Override
	public WebElement getDateBirth() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(dateOfBirth));
		logger.info("Date of Birth Box: " + dateOfBirth);
		return driver.findElement(dateOfBirth);
	}
	@Override
	public WebElement getPatientId() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(patientId));
		logger.info("Patient Id Box: " + patientId);
		return driver.findElement(patientId);
	}
	@Override
	public WebElement getNextButton() {
		logger.info("Next Button : " + nextButton);
		return driver.findElement(nextButton);
	}
	@Override
	public WebElement getBloodTestConfirmation() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(bloodTest));
		logger.info("Blood Test: " + bloodTest);
		return driver.findElement(bloodTest);
	}
	@Override
	public WebElement getIndicationStatement() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(indicationStatement));
		logger.info("Indication Statement: " + indicationStatement);
		return driver.findElement(indicationStatement);
	}
	@Override
	public WebElement getLYM() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(valueLYM));
		logger.info("LYM: " + valueLYM);
		return driver.findElement(valueLYM);
	}
	@Override
	public WebElement getNEUT() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(valueNEUT));
		logger.info("NEUT: " + valueNEUT);
		return driver.findElement(valueNEUT);
	}
	@Override
	public WebElement getALB() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(valueALB));
		logger.info("ALB: " + valueALB);
		return driver.findElement(valueALB);
	}
	@Override
	public WebElement getAST() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(valueAST));
		logger.info("AST: " + valueAST);
		return driver.findElement(valueAST);
	}
	@Override
	public WebElement getGGT() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(valueGGT));
		logger.info("GGT: " + valueGGT);
		return driver.findElement(valueGGT);
	}
	@Override
	public WebElement getLDH() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(valueLDH));
		logger.info("LDH: " + valueLDH);
		return driver.findElement(valueLDH);
	}
	@Override
	public WebElement getViewResult() {
		
		return driver.findElement(viewResult);
	}
	@Override
	public WebElement getDateofBirthMsg() {
		WebDriverWait wait = new WebDriverWait(driver, 15);
		wait.until(ExpectedConditions.elementToBeClickable(dateofBirthMsg));
		
		logger.info("dateofBirthMsg locator: " + dateofBirthMsg);
		return driver.findElement(dateofBirthMsg);
	}
	
	

}
