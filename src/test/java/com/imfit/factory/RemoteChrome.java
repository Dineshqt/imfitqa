package com.imfit.factory;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteChrome implements Browser {
	private static Logger logger = LoggerFactory.getLogger(RemoteChrome.class);
	private WebDriver driver;
	
	@Override
	public WebDriver getDefaultVersion() {
		try {
			ChromeOptions options = new ChromeOptions();
			driver = new RemoteWebDriver(new URL(System.getProperty("remote.browser.url")), options);
		
		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		}
		
		logger.info("Browser instance called - getDefaultVersion(): " + String.valueOf(driver.getClass()));

		return driver;
	}

	@Override
	public WebDriver getDefaultVersionUsingResolution(int height, int width) {
		try {
			ChromeOptions options = new ChromeOptions();
			driver = new RemoteWebDriver(new URL(System.getProperty("remote.browser.url")), options);
			driver.manage().window().setSize(new Dimension(height, width));
		
			logger.info("Browser instance called - getDefaultVersionUsingResolution(int height, int width): " + String.valueOf(driver.getClass()));
			
			Dimension initialSize = driver.manage().window().getSize();
			logger.info("Current screen resoultion width: " + initialSize.getWidth());
			logger.info("Current screen resoultion height: " + initialSize.getHeight());

		} catch (MalformedURLException e) {
			logger.error(e.getMessage());
		}
		
		return driver;
	}
}
