package com.imfit.factory;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Edge implements Browser {
	private static Logger logger = LoggerFactory.getLogger(Edge.class);
	private WebDriver driver;
	private String message = "You selected the browserType " + System.getProperty("browserType") + 
							 " but you didn't set the webdriver.chrome.driver system property. " + 
						 	 "For more details see the message at the end of the console output.";
	
	@Override
	public WebDriver getDefaultVersion() {
		try {
			driver = new EdgeDriver();
			driver.manage().window().maximize();
			
			logger.info("Browser instance called - getDefaultVersion(): " + String.valueOf(driver.getClass()));
			
		} catch(IllegalStateException e) {
			logger.error(message);
			throw e;
		}
		
		return driver;		
	}

	@Override
	public WebDriver getDefaultVersionUsingResolution(int height, int width) {
		try {
			driver = new EdgeDriver();
			driver.manage().window().setSize(new Dimension(height, width));
		
			logger.info("Browser instance called - getDefaultVersionUsingResolution(int height, int width): " + String.valueOf(driver.getClass()));
			
			Dimension initialSize = driver.manage().window().getSize();
			logger.info("Current screen resoultion width: " + initialSize.getWidth());
			logger.info("Current screen resoultion height: " + initialSize.getHeight());

		} catch(IllegalStateException e) {
			logger.error(message);
			throw e;
		}
		
		return driver;
	}
	
	public WebDriver getInstance() {
		return driver;
	}

}
