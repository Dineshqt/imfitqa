package com.imfit.factory;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SauceLabs implements Browser {
	private static Logger logger = LoggerFactory.getLogger(SauceLabs.class);
	private WebDriver driver;
	 
	@Override
	public WebDriver getDefaultVersion() {
		String userName = getPropertyValue("userName", System.getProperty("userName"));
		String accessKey = getPropertyValue("accessKey", System.getProperty("accessKey"));
		String url = "https://ondemand.saucelabs.com/wd/hub";

		MutableCapabilities sauceOpts = new MutableCapabilities();
        sauceOpts.setCapability("username", userName);
        sauceOpts.setCapability("accessKey", accessKey);
        
        //TODO Extract lib version from pom.xml
        sauceOpts.setCapability("seleniumVersion", "3.14.0");
        sauceOpts.setCapability("name", "IMFit QA UI Tests");
        sauceOpts.setCapability("build", "Crossbrowsing suite");
        
        //TODO See which values are required for different browsers
        ChromeOptions chromeOpts = new ChromeOptions();
        chromeOpts.setExperimentalOption("w3c", true);

        MutableCapabilities capabilities = new MutableCapabilities();
        capabilities.setCapability("sauce:options", sauceOpts);
        capabilities.setCapability("goog:chromeOptions", chromeOpts);
        
        String browserName = getPropertyValue("browserName", System.getProperty("browserName"));
        logger.info("Browser Name: " + browserName);
        capabilities.setCapability("browserName", browserName); //chrome
        
        String platformVersion = getPropertyValue("platformVersion", System.getProperty("platformVersion"));
        logger.info("platformVersion: " + platformVersion);
        capabilities.setCapability("platformVersion", platformVersion); //"Windows 10"
        
        String browserVersion = getPropertyValue("browserVersion", System.getProperty("browserVersion"));
        logger.info("browserVersion: " + browserVersion);
        capabilities.setCapability("browserVersion", browserVersion); //"latest"
        
		try {
			driver = new RemoteWebDriver(new URL(url), capabilities);
			
			logger.info("Browser instance called - getDefaultVersion(): " + String.valueOf(driver.getClass()));
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		return driver;
	}
	
	private String getPropertyValue(String name, String value){
		if (value == null){
			throw new RuntimeException("Env. variable " + name + " not provided.");
		}
		return value;
	}

	@Override
	public WebDriver getDefaultVersionUsingResolution(int height, int width) {
		// TODO Once we count with credentials we can continue working on the implementation of this object.
		return null;
	}
	
	public WebDriver getInstance() {
		return driver;
	}
}
