package com.imfit.factory;

import org.openqa.selenium.WebDriver;

public interface Browser {
	public WebDriver getDefaultVersion();
	public WebDriver getDefaultVersionUsingResolution(int height, int width);
}
