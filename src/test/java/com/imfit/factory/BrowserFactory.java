package com.imfit.factory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BrowserFactory implements AbstractFactory<Browser> {
	private static Logger logger = LoggerFactory.getLogger(BrowserFactory.class);
	
	@Override
	public Browser create(String browserType) {
		logger.info("Factory method called - create(String browserType): " + browserType);
		
		if(browserType.equals("LOCAL_CHROME")) {
			return new Chrome(); 
		}
		
		if(browserType.equals("REMOTE_CHROME")) {
			return new RemoteChrome(); 
		}
		
		if(browserType.equals("LOCAL_SAFARI")) {
			return new Safari(); 
		}
		
		if(browserType.equals("LOCAL_FIREFOX")) {
			return new Firefox(); 
		}
		
		if(browserType.equals("LOCAL_EDGE")) {
			return new Edge(); 
		}
		
		if(browserType.equals("LOCAL_IE")) {
			return new InternetExplorer(); 
		}
		
		if(browserType.equals("SAUCELABS")) {
			return new SauceLabs();
		}
		return null;
	}

}
