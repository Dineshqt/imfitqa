package com.imfit.helper;

import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PathGenerator {
	private String path;
	
	public PathGenerator setAbsolutePathFrom(String folder) {
		path = Paths.get(folder).toAbsolutePath().toString();
		return this;
	}
	
	public String getValue() {
		return path;
	}
	
	public String createAddingDateTime(String name) {
	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss");  
	   LocalDateTime now = LocalDateTime.now();  

	   return path + "/" + name + "_" + dtf.format(now) + ".png";
	}
}
