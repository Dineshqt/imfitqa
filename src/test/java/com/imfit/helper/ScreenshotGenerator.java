package com.imfit.helper;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScreenshotGenerator {
	private WebDriver driver;
	
	private static Logger logger = LoggerFactory.getLogger(ScreenshotGenerator.class);
	
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	
	public void saveIn(String filePath) {
		try {
			TakesScreenshot ss =((TakesScreenshot)driver);
			File srcFile = ss.getScreenshotAs(OutputType.FILE);
			File destFile = new File(filePath);
			
			logger.info("Saving screenshot");
			
			FileUtils.copyFile(srcFile, destFile);

		} catch (IOException e) {
			logger.error("Error saving screenshot: " + e.getMessage());
		}
	}
}
