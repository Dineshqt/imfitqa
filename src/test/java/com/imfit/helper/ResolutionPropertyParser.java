package com.imfit.helper;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResolutionPropertyParser {
	private static Logger logger = LoggerFactory.getLogger(ResolutionPropertyParser.class);
	private List<Integer> list;
	
	public ResolutionPropertyParser(String height, String width) {
		this.list = new ArrayList<Integer>();
		
		try {
			list.add(Integer.parseInt(height));
			list.add(Integer.parseInt(width));
				
			logger.info("Expected resolution screen: " + height + "x" + width);
		} catch(NumberFormatException e) {
			list.add(0);
			list.add(0);
			
			logger.info("Screen resolution values not defined or not declared properly. The browser will be executed with maximized screen.");
		}
	}
	
	public List<Integer> toInteger() {		
		return list;
	}
}
